package MyProject;

import java.util.Scanner;

public class ConvertAmount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final double ROUBLE_PER_GBPOUND = 81.91;

        int greatBritainPounds;
        double roubles, penny;
        int digit;


        //Получить сумму денег в британских фунтах
        System.out.print("Введите сумму денег в британских фунтах: ");
        greatBritainPounds = scanner.nextInt();

        //Отобразить сумму денег в британских фунтах с правильным окончанием
        System.out.print(greatBritainPounds);
        if (5 <= greatBritainPounds && greatBritainPounds <= 20)
            System.out.print(" британских фунтов равны ");
        else {
            digit = greatBritainPounds % 10;
            if (digit == 1)
                System.out.print(" британский фунт равен ");
            else if (2 <= digit && digit <= 4)
                System.out.print(" британских фунта равны ");
            else
                System.out.print(" британских фунтов равны ");
        }

        //Конвертировать сумму денег в российские рубли
        roubles = greatBritainPounds * ROUBLE_PER_GBPOUND;

        //Конвертировать сумму денег в российские копейки
        penny = greatBritainPounds * ROUBLE_PER_GBPOUND * 100 % 100;
        int pennyInt = (int) penny;

        //Отобразить сумму денег в российских рублях в пользу покупателя

        System.out.println(((int)(roubles * 100 / 100.0)) + " российского рубля и " + pennyInt + " копеек.");


    }
}